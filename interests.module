<?php

/**
 * @file interests.module
 * Module that allows users to select taxonomy terms from vocabularies as 'interests'.
 */

/**
 * Implementation of hook_help().
 */
function interests_help($path, $arg) {
  switch ($path) {
    case 'admin/help#interests':
      return t("<p>The interests module allows users to select taxonomy terms from vocabularies as 'interests'. Each time they visit a node with that taxonomy term applied to it the interest level bumps up one, or creates a new interest for them. Interests can be added/removed manually on an administration/user level or by the user themselves.</p>");
      break;
  }
}

/**
 * Implementation of hook_menu().
 */
function interests_menu() {
  $items = array();

  $items['user/%user/interests'] = array(
    'title' => t('My Interests'),
    'page callback' => 'interests_my_interests',
    'page arguments' => array(1),
    'access callback' => 'user_access',
    'access arguments' => array('access interests'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['user/%user/interests/edit'] = array(
    'title' => t('Change My Interests'),
    'page callback' => 'drupal_get_form',
    'page arguments' =>  array('interests_interests_form', 1),
    'access callback' => 'user_access',
    'access arguments' => array('access interests'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/user/interests'] = array(
    'title' => t('Interests'),
    'description' => t('Interests per user.'),
    'page callback' => 'interests_user_overview',
    'access callback' => 'user_access',
    'access arguments' => array('administer interests'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/user/interests/edit'] = array(
    'title' => t('Edit Interests'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('interests_interests_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer interests'),
    'type' => MENU_CALLBACK,
  );
  $items['interests/add'] = array(
    'title' => t('Add Interest'),
    'page callback' => 'interests_add_node',
    'access callback' => 'user_access',
    'access arguments' => array('access interests'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implementation of hook_perm().
 */
function interests_perm() {
  return array('access interests', 'administer interests');
}

/**
 * Implementation of hook_nodeapi().
 */
function interests_nodeapi(&$node, $op, $a3, $a4) {
  switch ($op) {
    case 'view':
      global $user;
      // Only do this for pages not teasers
      if ($user->uid && $a4) {
        $terms = taxonomy_node_get_terms($node);
        if ($terms) {
          foreach ($terms as $term) {
            if (interests_is_interest($user->uid, $term->tid)) {
              _interests_dynamic_add($term->tid, $user->uid);
            }
          }
        }
      }
  }
}

/**
 * Implementations of hook_form_alter().
 */
function interests_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'taxonomy_form_vocabulary') {
    $vid = (int) arg(5);
    $count = db_result(db_query("SELECT COUNT(*) FROM {interests_vocabs} WHERE vid = %d", $vid));
    $form['settings']['interests'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use as interests vocabulary'),
      '#default_value' => $count ? TRUE : FALSE,
      '#description' => t('If checked, this vocabulary will be used as an interests vocabulary.'),
    );

    $form['#submit'][] = 'interests_taxonomy_form_vocabulary_submit';
  }
}

/**
 * Submit function for the taxonomy_form_vocabulary form
 * Used to save whether or not the vocab is an interests vocab.
 */
function interests_taxonomy_form_vocabulary_submit($form, &$form_state) {
  $vocabs = _interests_get_vocabs();
  if ($form_state['values']['interests']) {
    if (!in_array($form_state['values']['vid'], $vocabs)) {
      db_query("INSERT INTO {interests_vocabs} VALUES (%d)", $form_state['values']['vid']);
    }
  }
  else {
    if (in_array($form_state['values']['vid'], $vocabs)) {
      db_query("DELETE FROM {interests_vocabs} WHERE vid = %d", $form_state['values']['vid']);
    }
  }
}

/**
 * Implementation of hook_link().
 */
function interests_link($type, $node = NULL, $teaser = FALSE) {
  $links = array();

  if ($type == 'node') {
    if (!$teaser) {
      if (user_access('access interests')) {
        $links['interests_add_interests'] = array(
          'title' => t('set as interest'),
          'href' => 'interests/add/' . $node->nid,
        );
      }
    }
  }
  return $links;
}

/**
 * My interests form (tab on my account page)
 */
function interests_interests_form($form_state, $user) {
  $output = l(t('Back to user list'), 'admin/user/interests');

  if (is_numeric(arg(4))) {
    $uid = arg(4);
    $default_values = _interests_get_interests($uid);
    $result = db_query("SELECT uid, name FROM {users} WHERE uid = %d", $uid);
    while ($row = db_fetch_object($result)) {
      $uname = $row->name;
    }
  }
  else {
    $default_values = _interests_get_interests($user->uid);
    $uid = $user->uid;
    $uname = $user->name;
  }

  $vocabs = _interests_get_vocabs();

  if (!empty($vocabs)) {
    foreach ($vocabs as $i => $voc) {
      $vocab_obj = taxonomy_vocabulary_load($voc);
      //Generate a taxonomy term hiearchy selector
      $tree = taxonomy_get_tree($voc);
      if ($tree) {
        foreach ($tree as $term) {
          $choice = new stdClass();
          $choice->option = array($term->tid => str_repeat('-', $term->depth) . $term->name);
          $termlist[$vocab_obj->name][] = $choice;
        }
      }
    }

    $form['interest_terms'] = array(
      '#type' => 'select',
      '#title' => t('Interests for ' . $uname),
      '#multiple' => TRUE,
      '#default_value' => $default_values,
      '#options' => $termlist,
    );
    $form['uid'] = array(
      '#type' => 'hidden',
      '#value' => $uid,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save',
    );
  }
  else {
    $form['novocabs'] = array(
      '#value' => t('No vocabularies have been assigned as a Interest vocabulary.'),
    );
  }
  return $form;
}

/**
 * My interests form submit
 */
function interests_interests_form_submit($form, &$form_state) {
  $current_result = db_query("SELECT * FROM {interests} WHERE uid = %d", $form_state['values']['uid']);
  while ($current_row = db_fetch_array($current_result)) {
    $current_list[] = array('tid' => $current_row['tid'], 'uid' => $current_row['uid']);
  }

  if ($current_list) {
    foreach ($current_list as $i => $current_term) {
      if (array_key_exists($current_term['tid'], $form_state['values']['interest_terms'])) {
        unset($form_state['values']['interest_terms'][$current_term['tid']]);
        unset($current_list[$i]);
      }
    }
  }
  foreach ($form_state['values']['interest_terms'] as $tid) {
    $insert_result = db_query("INSERT INTO {interests} values (%d, %d, 0)", $form_state['values']['uid'], $tid);
  }
  if ($current_list) {
    foreach ($current_list as $current) {
      $delete_result = db_query("DELETE FROM {interests} WHERE uid = %d AND tid = %d", $form_state['values']['uid'], $current['tid']);
    }
  }

  drupal_set_message('Interests updated');

  $form_state['redirect'] = 'user/' . $form_state['values']['uid'] . '/interests';
}

function interests_user_overview() {
  $result = db_query('SELECT DISTINCT u.uid, u.name FROM {users} u WHERE u.uid != 0 ORDER BY u.name');
  while ($row = db_fetch_object($result)) {
    $table_row[] = array($row->name, l('Edit Interests', 'admin/user/interests/edit/' . $row->uid));
  }
  $output = theme('table', array('Username', 'Edit'), $table_row);

  return $output;
}

/**
 * Get the users interests
 */
function _interests_get_interests($uid) {
  $rows = array();

  if ($uid) {
    $result = db_query("SELECT tid FROM {interests} WHERE uid = %d", $uid);
    while ($row = db_fetch_array($result)) {
      $rows[] = $row['tid'];
    }
  }

  return $rows;
}

/**
 * Return whether or not the term is one of the users interests.
 */
function interests_is_interest($uid, $tid) {
  if (!$uid || !$tid) {
    return FALSE;
  }

  $interests = _interests_get_interests($uid);

  if (in_array($tid, $interests)) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Gets a list of interests vocabs.
 */
function _interests_get_vocabs() {
  $vocabs = array();
  $result = db_query("SELECT * FROM {interests_vocabs}");
  while ($row = db_fetch_object($result)) {
    $vocabs[$row->vid] = $row->vid;
  }
  return $vocabs;
}

/**
 * Adds a nodes terms to the current users interests.
 */
function interests_add_node() {
  global $user;
  if ($user->uid) {
    $current_vocabs = _interests_get_vocabs();
    $node = node_load(arg(2));
    $terms = taxonomy_node_get_terms($node);
    if ($terms) {
      foreach ($terms as $term) {
        if (interests_is_interest($user->uid, $term->tid)) {
          _interests_dynamic_add($term->tid, $user->uid);
        }
      }
    }
  }

  drupal_goto('node/' . $node->nid);
}

/**
 * Adds a new interest tid to the database for the given user
 * If the interest is already there it adds one to the count for that interest.
 */
function _interests_dynamic_add($tid, $uid) {
  if (!$uid || !$tid) {
    return FALSE;
  }

  $any_rows = FALSE;

  $result = db_query("SELECT * FROM {interests} WHERE uid = %d AND tid = %d", $uid, $tid);
  while ($row = db_fetch_array($result)) {
    $any_rows = TRUE;
    $counter = $row['counter'] + 1;
    db_query("UPDATE {interests} SET counter = %d WHERE uid = %d AND tid = %d", $counter, $uid, $tid);
  }

  if (!$any_rows) {
    db_query("INSERT INTO {interests} VALUES (%d, %d, 1)", $uid, $tid);
  }
}

/**
 * Displays list of users that have most interest matches with you
 * and list of nodes that best match your interests.
 */
function interests_my_interests($user) {
  $in = '';
  $query_vars = array();

  if (module_exists('blog')) {
    $blog_enabled = TRUE;
  }

  $output = l(t('Change My Interests'), 'user/' . $user->uid . '/interests/edit');

  // Do the selection of users where at least one tag matches, order by number of matches - Limited to 10

  $my_tags = db_query("SELECT tid FROM {interests} WHERE uid = %d", $user->uid);

  while ($row = db_fetch_array($my_tags)) {
    $in .= $in ? ', %d' : '%d';
    $query_vars[] = $row['tid'];
  }
  if ($in != '') {
    $query_vars[] = $user->uid;
    $sql = "SELECT DISTINCT u.uid, u.name, SUM(i.counter) AS total_count
                              FROM {interests} i, {users} u
                              WHERE u.uid = i.uid
                              AND i.tid IN ($in)
                              AND i.uid NOT IN (%d, 0)
                              GROUP BY u.uid, u.name
                              ORDER BY total_count DESC
                              LIMIT 10";
    $result_users = db_query("SELECT DISTINCT u.uid, u.name, SUM(i.counter) AS total_count
                              FROM {interests} i, {users} u
                              WHERE u.uid = i.uid
                              AND i.tid IN ($in)
                              AND i.uid NOT IN (%d, 0)
                              GROUP BY u.uid, u.name
                              ORDER BY total_count DESC
                              LIMIT 10", $query_vars);

    $table_rows = array();
    while ($row = db_fetch_array($result_users)) {
      if ($blog_enabled) {
        $table_rows[] = array(
          'username' => $row['name'],
          'blog' => l(t('View Blog'), 'blog/' . $row['uid']),
          'profile' => l(t('View Profile'), 'user/' . $row['uid'])
        );
      }
      else {
        $table_rows[] = array(
          'username' => $row['name'],
          'profile' => l(t('View Profile'), 'user/' . $row['uid'])
        );
      }
    }

    if (!empty($table_rows)) {
      $output .= "<h3>Users with similar interests</h3>";
      if ($blog_enabled) {
        $output .= theme('table', array('Username', 'Blog', 'Profile'), $table_rows);
      }
      else {
        $output .= theme('table', array('Username', 'Profile'), $table_rows);
      }
    }
  }
  else {
    $output .= "<p>You haven't specified any interests yet.</p>";
  }

  // Do the selection of nodes where tags match
  $types = db_query("SELECT nt.type, nt.name FROM {node_type} nt");
  while ($type = db_fetch_array($types)) {
    $nodes = db_query("SELECT n.nid, n.title, SUM(i.counter) AS total_count
                       FROM {interests} i, {term_node} tn, {node} n
                       WHERE n.nid = tn.nid
                       AND i.tid = tn.tid
                       AND i.uid = %d
                       AND n.type = '%s'
                       GROUP BY n.nid, n.title
                       ORDER BY total_count DESC, n.changed DESC
                       LIMIT 5", $user->uid, $type['type']);

    $list = array();
    while ($node = db_fetch_array($nodes)) {
      $list[] = l($node['title'], 'node/' . $node['nid']);
    }
    if (!empty($list)) {
      $output .= theme('item_list', $list, t($type['name']), 'ul');
    }
  }

  return $output;
}

/**
 * Creates the content for the interests block.
 */
function _interests_user_suggestions() {
  global $user;
  $output = '';

  if ($user->uid == 0) {
    $output .= t("This site uses interest tagging for authenticated users");
  }
  else {
    if (module_exists('tagadelic')) {
      $result = db_query("SELECT i.counter AS count, t.tid, t.name, t.vid, t.weight
                          FROM {interests} i, {term_data} t
                          WHERE t.tid = i.tid
                          AND i.uid = %d
                          ORDER BY t.name DESC", $user->uid);
      $tags = tagadelic_build_weighted_tags($result, $steps = 6);
      $output .= theme('tagadelic_weighted', $tags);
    }
  }
  return $output;
}

/**
 * Implementation of hook_block().
 */
function interests_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0]['info'] = t('Interests terms tag block');
      $blocks[0]['cache'] = BLOCK_NO_CACHE;
      return $blocks;
    case 'configure':
      $form = array();
      return $form;
    case 'view':
      switch ($delta) {
        case 0:
          $block = array(
            'subject' => 'Your Interests',
            'content' => _interests_user_suggestions()
          );
          return $block;
      } // End delta switch
  } // End op switch
}
